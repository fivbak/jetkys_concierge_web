<?php
require 'common_include.php';
$title="";

$url = "";

$action=$_GET['action'];

if($action=="login"){
  $url = API_PATH.API_GOSSIP_AUTH;
  $user_email=$_POST['user_email'];
  $user_pwd=$_POST['user_pwd'];
  $contact=$_POST['contact'];

  $post_data['user_email'] = $user_email;
  $post_data['user_pwd'] = $user_pwd;

  $o = "";
  foreach ( $post_data as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
  $post_data = substr($o,0,-1);
  $res = request_post($url, $post_data);
  $obj = json_decode($res);

  $result = $obj->{'result'};
  $msg = $obj->{'msg'};
  if($result!="0"){
    if($contact=="1"){
      $expire = time() + 30*86400;
//      setcookie ("user_id",$obj->{'data'}->{'user_id'}, $expire);
//      setcookie ("user_nick",$obj->{'data'}->{'user_nick'}, $expire);
      setcookie ("user_id",1, $expire);
      setcookie ("user_nick","テスト君", $expire);
    }else{
//      $_SESSION['user_id']=$obj->{'data'}->{'user_id'};
//      $_SESSION['user_nick']=$obj->{'data'}->{'user_nick'};
      $_SESSION['user_id']=1;
      $_SESSION['user_nick']="テスト君";
    }
    header("Location:".HOME_PAGE);
  }

}
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <div class="col-md-4">
        <div class="nav--signin">
          <div class="nav--signin__main">
            <label class="visible-xs">ユーザーID</label>
            <input type="text" name="user_email" maxlength="100" class="nav--signin__input" placeholder="ユーザーID" value="<?php echo $user_email;?>" />
            <label class="visible-xs">パスワード</label>
            <input type="password" placeholder="パスワード" class="nav--signin__input"/>
            <input type="password" name="user_pwd" placeholder="パスワード" class="nav--signin__input" value="<?php echo $user_pwd;?>" />
            <input name="contact" type="checkbox" value="1" id="checkbox01" checked />
            <label for="checkbox01" class="checkbox">ログイン状態を保持する</label>
            <div class="btn--signin">
              <a href="#">ログイン</a>
            </div>
            <div class="nav--signin__others">
              <a href="#" class="">パスワードを忘れた方</a><br class="hidden-xs">
              <a href="#">ログインできない方</a><br class="hidden-xs">
              <a href="./help.php">ヘルプ</a>
            </div>
          </div>
          <div class="nav--signin__sns">
            <div class="nav--signin__sns__item hidden-xs">
              <img src="img/icon_google.jpg">
              <a href="#">Google+アカウントでログイン</a>
            </div>
            <div class="nav--signin__sns__item">
              <img src="img/icon_twitter.png">
              <a href="#">Twitterアカウントでログイン</a>
            </div>
            <div class="nav--signin__sns__item">
              <img src="img/icon_facebook.png">
              <a href="#">Facebookアカウントでログイン</a>
            </div>
          </div>
          <div class="btn--signup">
            <a href="./signup.php">無料登録</a>
          </div>
        </div>
      </div>


      <!-- main -->
      <div class="col-md-8 hidden-xs hidden-sm">
        <div class="top-main__news">
          <div class="top-main__news__item">
            <p class="date">2月26日</p>
            <a href="#">テキストテキストテキストテキスト</a>
          </div>
          <div class="top-main__news__item">
            <p class="date">2月26日</p>
            <a href="#">テキストテキストテキストテキスト</a>
          </div>
          <div class="top-main__news__item">
            <p class="date">2月26日</p>
            <a href="#">テキストテキストテキストテキスト</a>
          </div>
          <div class="top-main__news__item">
            <p class="date">2月26日</p>
            <a href="#">テキストテキストテキストテキスト</a>
          </div>
        </div>

        <div class="top-main__genre">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="top-main__genre__btn">
                <a href="#">
                  <img src="img/btn_feeling.png">
                </a>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="top-main__genre__btn">
                <a href="#">
                  <img src="img/btn_uwaki.png">
                </a>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="top-main__genre__btn">
                <a href="#">
                  <img src="img/btn_tech.png">
                </a>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="top-main__genre__btn">
                <a href="#">
                  <img src="img/btn_grow.png">
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="top-main__users">
          <div class="row">
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-12">
              <a href="#" class="top-main__users__more pull-right">もっとみる</a>
            </div>
          </div>
        </div>
        <div class="top-main__comic">
          <p>マンガ①</p>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
<script src="js/switch.js"></script>
</body>
</html>
