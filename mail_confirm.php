<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8 inbox">
        <div class="box noborder--bottom--sp">
          <div class="box__header hidden-xs">
            <p>▶里中 今日子</p>
          </div>
          <div class="box__header box__header__icon--left--sp visible-xs">
            <a href="./mails.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>里中 今日子</p>
            <a href="#">
              <div class="box__header__icon--right visible-xs box__header__icon--comment auto-width">
                <img src="img/comment.png">
              </div>
            </a>
          </div>
          <div class="box__body confirm-mail">
            <div class="box__body compose-mail__container confirm-mail__container">
              <p class="confirm-mail__title">タイトル</p>
              <p class="confirm-mail__content">テキストテキストテキストテキストテキストテキスト</p>
            </div>
            <div class="compose-mail__attachments">
              <a href="#" class="attached">
                <img src="img/icon_camera.png">
                <img src="img/icon_clip2.png" class='icon-clip'>
              </a>
              <a href="#" class="attached hidden-xs">
                <img src="img/icon_video.png">
                <img src="img/icon_clip2.png" class='icon-clip'>
              </a>
            </div>
          </div>
          <div class="btn-back visible-xs-inline-block">
            <a href="#">戻る</a>
          </div>
          <div class="inbox__mail__reply">
            <a href="./mail_sent.php">送信</a>
          </div>
        </div>

        <div class="btn-back hidden-xs">
          <a href="#">戻る</a>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
<script src="js/tab.js"></script>
</body>
</html>
