<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
  <?php include "common_header.php"; ?>
  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>

      <!-- main -->
      <div class="col-md-8">
        <div class="box noborder--bottom--sp">
          <div class="box__header box__header__icon--left--sp">
            <a href="./mails.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>里中 今日子</p>
          </div>
          <div class="box__body profile divination-show">
            <a href="#">
              <div class="btn--divination">
                <p class="btn--divination__title">◯◯◯鑑定</p>
                <div class="btn--divination__round"></div>
                <div class="btn--divination__price">
                  <img src="img/price.png">
                </div>
              </div>
            </a>

            <div class="profile__description profile__box">
              <div class="profile__box__header">
                <p>プロフィール</p>
              </div>
              <div class="profile__box__body">
                <div class="profile__box__body__item profile__box__body__item--description">
                  <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                </div>
                <div class="profile__box__body__item profile__box__label">
                  <label class="profile__box__label--price">料金</label>
                  <p>1000<span class="small">円</span></p>
                </div>
                <div class="profile__box__body__item profile__box__label">
                  <label class="profile__box__label--responce">返答期間</label>
                  <p>48<span class="small">時間以内</span></p>
                </div>
              </div>
            </div>

          </div> <!-- box__body -->
          <div class="btn--default mg-top-10">
            <a href="#">お申し込み</a>
          </div>
        </div> <!-- box -->
      </div> <!-- main -->

    </div>
  </div>

  <?php include "common_footer.php"; ?>
</body>
</html>
