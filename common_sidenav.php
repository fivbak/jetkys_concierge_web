    <div class="col-md-4 hidden-xs">
        <div class="sidenav">
            <div class="sidenav__profile sidenav__item">
                <div class="sidenav__item__header">
                    <p>名前</p>
                </div>
                <div class="sidenav__item__body">
                    <div class="sidenav__profile__picture">
                        <img src="img/icon_silette.png">
                    </div>
                    <div class="sidenav__profile__names">
                        <p class="name">名前</p>
                        <p>ID:12356</p>
                        <p>所持pt:12345</p>
                    </div>
                    <div class="sidenav__profile__text">
                        <p>テキストテキストテキストテキストテキストテキストテキスト</p>
                    </div>
                </div>
            </div>

            <div class="sidenav__my-saliencies sidenav__item">
                <div class="sidenav__item__header">
                    <p>マイサリエンシー</p>
                </div>
                <div class="sidenav__item__body">
                    <a href="#">
                        <div class="sidenav__users">
                            <div class="sidenav__users__picture">
                                <img src="img/icon_woman.png">
                            </div>
                            <div class="sidenav__users__names">
                                <p class="name">里中 今日子</p>
                                <p>テキストテキストテキストテキスト</p>
                            </div>
                            <div class="sidenav__users__comment">
                                <img src="img/icon_comment_lg.png">
                                <div class="notice">
                                </div>
                            </div>
                            <div class="sidenav__users__mail">
                                <img src="img/icon_mail_lg.png">
                                <div class="notice">
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="sidenav__users">
                            <div class="sidenav__users__picture">
                                <img src="img/icon_woman.png">
                            </div>
                            <div class="sidenav__users__names">
                                <p class="name">里中 今日子</p>
                                <p>テキストテキストテキストテキスト</p>
                            </div>
                            <div class="sidenav__users__comment">
                                <img src="img/icon_comment_lg.png">
                            </div>
                            <div class="sidenav__users__mail">
                                <img src="img/icon_mail_lg.png">
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="sidenav__users">
                            <div class="sidenav__users__picture">
                                <img src="img/icon_man.png">
                            </div>
                            <div class="sidenav__users__names">
                                <p class="name">里中 今日子</p>
                                <p>テキストテキストテキストテキスト</p>
                            </div>
                            <div class="sidenav__users__comment">
                                <img src="img/icon_comment_lg.png">
                            </div>
                            <div class="sidenav__users__mail">
                                <img src="img/icon_mail_lg.png">
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="sidenav__my-users sidenav__item">
                <div class="sidenav__item__header">
                    <p>マイユーザー</p>
                </div>
                <div class="sidenav__item__body">
                    <a href="#">
                        <div class="sidenav__users">
                            <div class="sidenav__users__picture">
                                <img src="img/icon_woman.png">
                            </div>
                            <div class="sidenav__users__names">
                                <p class="name">里中 今日子</p>
                                <p>テキストテキストテキストテキスト</p>
                            </div>
                            <div class="sidenav__users__comment">
                                <img src="img/icon_comment_lg.png">
                                <div class="notice">
                                </div>
                            </div>
                            <div class="sidenav__users__mail">
                                <img src="img/icon_mail_lg.png">
                                <div class="notice">
                                </div>
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="sidenav__users">
                            <div class="sidenav__users__picture">
                                <img src="img/icon_woman.png">
                            </div>
                            <div class="sidenav__users__names">
                                <p class="name">里中 今日子</p>
                                <p>テキストテキストテキストテキスト</p>
                            </div>
                            <div class="sidenav__users__comment">
                                <img src="img/icon_comment_lg.png">
                            </div>
                            <div class="sidenav__users__mail">
                                <img src="img/icon_mail_lg.png">
                            </div>
                        </div>
                    </a>
                    <a href="#">
                        <div class="sidenav__users">
                            <div class="sidenav__users__picture">
                                <img src="img/icon_man.png">
                            </div>
                            <div class="sidenav__users__names">
                                <p class="name">里中 今日子</p>
                                <p>テキストテキストテキストテキスト</p>
                            </div>
                            <div class="sidenav__users__comment">
                                <img src="img/icon_comment_lg.png">
                            </div>
                            <div class="sidenav__users__mail">
                                <img src="img/icon_mail_lg.png">
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div> <!-- sidenav -->
    </div>