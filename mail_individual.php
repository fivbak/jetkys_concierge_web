<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8 my-saliency">
        <div class="box">
          <div class="box__header box__header__icon--left--sp">
            <a href="./mails.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>里中 今日子</p>
            <a href="#">
              <div class="box__header__icon--right visible-xs box__header__icon--comment auto-width">
                <img src="img/comment.png">
              </div>
            </a>
          </div>
          <div class="box__body list">
            <a href="./mail_show.php">
              <div class="list__mails">
                <div class="list__mails__mail-icon">
                  <img src="img/icon_mail_lg.png" class="unread-mail">
                </div>
                <div class="list__mails__names">
                  <p class="name">里中 今日子</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__mails__pictures pull-right">
                  <img src="img/icon_clip.png" class="icon__clip">
                  <img src="img/icon_woman.png" class="icon__picture">
                </div>
              </div>
            </a>
            <a href="./mail_show.php">
              <div class="list__mails">
                <div class="list__mails__mail-icon">
                  <img src="img/icon_mail_open_lg.png">
                </div>
                <div class="list__mails__names">
                  <p class="name">里中 今日子</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__mails__pictures pull-right">
                  <img src="img/icon_clip.png" class="icon__clip">
                  <img src="img/icon_woman.png" class="icon__picture">
                </div>
              </div>
            </a>
            <a href="./mail_show.php">
              <div class="list__mails">
                <div class="list__mails__mail-icon">
                  <img src="img/icon_mail_open_lg.png">
                </div>
                <div class="list__mails__names">
                  <p class="name">里中 今日子</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__mails__pictures pull-right">
                  <img src="img/icon_woman.png" class="icon__picture">
                </div>
              </div>
            </a>
          </div>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
<script src="js/tab.js"></script>
</body>
</html>
