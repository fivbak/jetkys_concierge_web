<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8">
        <div class="box">
          <div class="box__header">
            <p>検索</p>
          </div>
          <div class="box__body list">
            <div class="box__body__label box__body__label--center box__body__label--round">
              <a href="./search.php">
                <label>再検索</label>
              </a>
            </div>
            <a href="#">
              <div class="list__users">
                <div class="list__users__picture">
                  <img src="img/icon_woman_60.png">
                </div>
                <div class="list__users__names">
                  <p class="name">里中 今日子</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__users__arrow">
                  <img src="img/arrow_right.png">
                </div>
              </div>
            </a>
            <a href="#">
              <div class="list__users">
                <div class="list__users__picture">
                  <img src="img/icon_woman_60.png">
                </div>
                <div class="list__users__names">
                  <p class="name">里中 今日子</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__users__arrow">
                  <img src="img/arrow_right.png">
                </div>
              </div>
            </a>
            <a href="#">
              <div class="list__users">
                <div class="list__users__picture">
                  <img src="img/icon_woman_60.png">
                </div>
                <div class="list__users__names">
                  <p class="name">里中 今日子</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__users__arrow">
                  <img src="img/arrow_right.png">
                </div>
              </div>
            </a>
            <a href="#">
              <div class="list__users">
                <div class="list__users__picture">
                  <img src="img/icon_woman_60.png">
                </div>
                <div class="list__users__names">
                  <p class="name">里中 今日子</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__users__arrow">
                  <img src="img/arrow_right.png">
                </div>
              </div>
            </a>
          </div>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
</body>
</html>
