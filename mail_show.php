<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8 inbox">
        <div class="box inbox__mail noborder--bottom--sp">
          <div class="box__header hidden-xs">
            <p>▶里中 今日子</p>
          </div>
          <div class="box__header box__header__icon--left--sp visible-xs">
            <a href="./mails.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>里中 今日子</p>
            <a href="#">
              <div class="box__header__icon--right visible-xs box__header__icon--comment auto-width">
                <img src="img/comment.png">
              </div>
            </a>
          </div>
          <div class="box__body inbox__mail__content">
            <div class="inbox__mail__content__header">
              <p>タイトル</p>
            </div>
            <div class="inbox__mail__content__sub-header">
              <p>2016/1/1 12:12:00</p>
            </div>
            <div class="inbox__mail__content__body">
              <div class="inbox__mail__content__text">
                <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
              </div>
              <div class="row inbox__mail__content__images">
                <div class="col-sm-6 col-xs-6">
                  <img src="http://dummyimage.com/500x400/999999/ffffff&text=Dummy+image">
                </div>
                <div class="col-sm-6 col-xs-6">
                  <img src="http://dummyimage.com/500x400/999999/ffffff&text=Dummy+image">
                </div>
              </div>
            </div>
          </div>
          <div class="inbox__mail__reply">
            <a href="./mail_new.php">返信</p></a>
          </div>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
<script src="js/tab.js"></script>
</body>
</html>
