<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8 my-saliency">
        <div class="box">
          <div class="box__header">
            <p>メール</p>
          </div>
          <div class="box__tab">
            <div class="box__tab__item box__tab__item--active box__tab--first">
              <p class="font--10--sp">受信ボックス</p>
            </div>
            <div class="box__tab__item box__tab--second">
              <p class="font--10--sp">送信ボックス</p>
            </div>
          </div>

          <!-- 受信ボックス -->
          <div class="box__body list inbox box__body--first">
            <a href="./mail_individual.php">
              <div class="list__mails">
                <div class="list__mails__mail-icon">
                  <img src="img/icon_mail_lg.png" class="unread-mail">
                </div>
                <div class="list__mails__names">
                  <p class="name">タイトル</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__mails__pictures pull-right">
                  <img src="img/icon_clip.png" class="icon__clip">
                  <img src="img/icon_woman.png" class="icon__picture">
                </div>
              </div>
            </a>
            <a href="./mail_individual.php">
              <div class="list__mails">
                <div class="list__mails__mail-icon">
                  <img src="img/icon_mail_open_lg.png">
                </div>
                <div class="list__mails__names">
                  <p class="name">タイトル</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__mails__pictures pull-right">
                  <img src="img/icon_clip.png" class="icon__clip">
                  <img src="img/icon_woman.png" class="icon__picture">
                </div>
              </div>
            </a>
            <a href="./mail_individual.php">
              <div class="list__mails">
                <div class="list__mails__mail-icon">
                  <img src="img/icon_mail_open_lg.png">
                </div>
                <div class="list__mails__names">
                  <p class="name">タイトル</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__mails__pictures pull-right">
                  <img src="img/icon_woman.png" class="icon__picture">
                </div>
              </div>
            </a>
          </div>

          <!-- 送信ボックス -->
          <div class="box__body list sentbox box__body--second">
            <a href="./mail_individual.php">
              <div class="list__mails">
                <div class="list__mails__mail-icon">
                  <img src="img/icon_mail_open_lg.png">
                </div>
                <div class="list__mails__names">
                  <p class="name">タイトル</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__mails__pictures pull-right">
                  <img src="img/icon_clip.png" class="icon__clip">
                  <img src="img/icon_woman.png" class="icon__picture">
                </div>
              </div>
            </a>
            <a href="./mail_individual.php">
              <div class="list__mails">
                <div class="list__mails__mail-icon">
                  <img src="img/icon_mail_open_lg.png">
                </div>
                <div class="list__mails__names">
                  <p class="name">タイトル</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__mails__pictures pull-right">
                  <img src="img/icon_clip.png" class="icon__clip">
                  <img src="img/icon_woman.png" class="icon__picture">
                </div>
              </div>
            </a>
            <a href="./mail_individual.php">
              <div class="list__mails">
                <div class="list__mails__mail-icon">
                  <img src="img/icon_mail_open_lg.png">
                </div>
                <div class="list__mails__names">
                  <p class="name">タイトル</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__mails__pictures pull-right">
                  <img src="img/icon_woman.png" class="icon__picture">
                </div>
              </div>
            </a>
          </div>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
<script src="js/tab.js"></script>
</body>
</html>
