<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8 chat">
        <div class="box box--default noborder--bottom--sp">
          <div class="box__header box__header__icon--left--sp">
            <a href="./mails.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>里中 今日子</p>
            <a href="#">
              <div class="box__header__icon--right visible-xs box__header__icon--comment auto-width">
                <img src="img/comment.png">
              </div>
            </a>
          </div>
          <div class="box__body">
            <div class="chat__body">
              <p class="chat__body__date">2015年12月31日</p>
              <div class="chat__item chat__left">
                <div class="chat__left__image">
                  <img src="img/icon_woman_60.png">
                </div>
                <div class="chat__left__text">
                  <img src="img/balloon_arrow_left.png">
                  <div class="chat__left__text__main">
                    <p>こんにちは。<br>ご相談したい内容を教えて下さい。</p>
                  </div>
                </div>
              </div>
              <div class="chat__item chat__right pull-right">
                <div class="chat__item__unread-mark">
                  <p>未</p>
                </div>
                <div class="chat__right__text">
                  <div class="chat__right__text__main">
                    <p>こんにちは！<br>実は先日、会社で・・・</p>
                  </div>
                  <img src="img/balloon_arrow_right.png">
                </div>
                <div class="chat__right__image">
                  <img src="img/icon_silette.png">
                </div>
              </div>

              <!-- overlay for points
              <div class="chat__body--overlay">
                <div class="chat__dialog">
                  <div class="chat__dialog__item">
                    <p>メッセージの送信に<br>５ポイント消費します</p>
                  </div>
                  <div class="chat__dialog__item">
                    <a>はい、以降表示しない</a>
                  </div>
                  <div class="chat__dialog__item">
                    <a>はい</a>
                  </div>
                  <div class="chat__dialog__item">
                    <a>いいえ</a>
                  </div>
                </div>
              </div> -->

              <!-- overlay for selection-->
              <div class="chat__body--overlay">
                <div class="chat__dialog">
                  <div class="chat__dialog__item header-normal">
                    <p>選択してください</p>
                  </div>
                  <div class="chat__dialog__item">
                    <a>送信</a>
                  </div>
                  <div class="chat__dialog__item">
                    <a>削除</a>
                  </div>
                  <div class="chat__dialog__item">
                    <a>キャンセル</a>
                  </div>
                </div>
              </div>

              <!-- overlay for point shortage 
              <div class="chat__body--overlay">
                <div class="chat__dialog chat__dialog--two">
                  <div class="chat__dialog__item">
                    <p>ポイントが不足しています。<br>ポイントを購入しましょう。</p>
                  </div>
                  <div class="chat__dialog__item first">
                    <a>いいえ</a>
                  </div>
                  <div class="chat__dialog__item second">
                    <a>はい</a>
                  </div>
                </div>
              </div> -->

            </div>
            <div class="chat__footer">
              <div class="chat__footer__textarea">
                <textarea placeholder="ここにメッセージを入力してください。"></textarea>
              </div>
              <div class="chat__footer__btn">
                <a href="#">
                  <img src="img/icon_send.png" class="hidden-xs">
                  <img src="img/icon_send_pink.png" class="visible-xs">
                  <p class="hidden-xs">送信</p>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
<script src="js/dialog.js"></script>
</body>
</html>
