$(function() {
  $('.chat__footer__btn').click(function() {
    $('.chat__body--overlay').show();
  });

  $('.chat__dialog__item a').click(function() {
    $('.chat__body--overlay').hide();
  });
});
