$(function() {
  $('.box__body--second').hide();

  $('.box__tab--first').click(function() {
    $('.box__tab--second').removeClass('box__tab__item--active');
    $(this).addClass('box__tab__item--active');
    $('.box__body--first').show();
    $('.box__body--second').hide();
  });

  $('.box__tab--second').click(function() {
    $('.box__tab--first').removeClass('box__tab__item--active');
    $(this).addClass('box__tab__item--active');
    $('.box__body--second').show();
    $('.box__body--first').hide();
  });
});
