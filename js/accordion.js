$(function() {
  $('.help__list__content').hide();

  $('.box--round__header__icon--right').click(function() {
    $(this).parent().siblings().toggle();
    if ($(this).hasClass("opened")) {
      $(this).removeClass("opened");
      $(this).rotate(0);
    } else {
      $(this).addClass("opened");
      $(this).rotate(180);
    }
  });
});
