$(function() {
  $('.onoffswitch-label').click(function() {
    if ($(this).hasClass("on")) {
      $(this).removeClass("on");
      $(this).addClass("off");
    } else {
      $(this).removeClass("off");
      $(this).addClass("on");
    }
  });
});
