<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8 inbox">
        <div class="box noborder--bottom--sp mail-offer">
          <div class="box__header hidden-xs">
            <p>▶里中 今日子</p>
          </div>
          <div class="box__header box__header__icon--left--sp visible-xs">
            <a href="./mails.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>里中 今日子</p>
            <a href="#">
              <div class="box__header__icon--right visible-xs box__header__icon--comment">
                <img src="img/icon_mail_sp.png">
              </div>
            </a>
          </div>
          <div class="box__body compose-mail">
            <div class="box__body compose-mail__container">
              <input class="compose-mail__title" placeholder="タイトル">
              <textarea class="compose-mail__content" placeholder="内容を入力してください。"/></textarea>
            </div>
            <div class="compose-mail__attachments hidden-xs">
              <a href="#">
                <img src="img/icon_camera.png">
              </a>
              <a href="#">
                <img src="img/icon_video.png">
              </a>
            </div>
            <div class="compose-mail__other-info">
              <div class="compose-mail__other-info__item">
                <label>生年月日</label>
                <div class="select-box01 compose-mail__other-info--birthyear">
                  <select>
                    <option value=""></option>
                    <option value="">1895</option>
                    <option value="">1896</option>
                    <option value="">1897</option>
                    <option value="">1898</option>
                    <option value="">1899</option>
                  </select>
                </div>
                <p class="text--unit">年</p>
                <div class="select-box01 compose-mail__other-info--birthmonth">
                  <select>
                    <option value=""></option>
                    <option value="">1</option>
                    <option value="">2</option>
                    <option value="">3</option>
                    <option value="">4</option>
                    <option value="">5</option>
                  </select>
                </div>
                <p class="text--unit">月</p>
                <div class="select-box01 compose-mail__other-info--birthday">
                  <select>
                    <option value=""></option>
                    <option value="">1</option>
                    <option value="">2</option>
                    <option value="">3</option>
                    <option value="">4</option>
                    <option value="">5</option>
                  </select>
                </div>
                <p class="text--unit">日</p>
              </div>
              <div class="compose-mail__other-info__item">
                <label>血液型</label>
                <div class="select-box01 compose-mail__other-info--bloodtype">
                  <select>
                    <option value=""></option>
                    <option value="">A</option>
                    <option value="">B</option>
                    <option value="">O</option>
                    <option value="">AB</option>
                    <option value="">不明</option>
                  </select>
                </div>
                <p class="text--unit">型</p>
              </div>
            </div>
          </div>
          <div class="inbox__mail__reply hidden-xs">
            <a href="./mail_confirm.php">確定</p></a>
          </div>
        </div>
      </div> <!-- main -->
      <div class="visible-xs compose-mail__footer">
        <a href="#" class="compose-mail__footer__camera">
          <img src="img/icon_camera.png">
        </a>
        <a href="./mail_confirm.php" class="compose-mail__footer__action pull-right">確定</p></a>
      </div>

    </div>
  </div>

<?php include "common_footer.php"; ?>
<script src="js/tab.js"></script>
</body>
</html>
