<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8">
        <div class="box noborder--bottom--sp">
          <div class="box__header box__header__icon--left--sp">
            <a href="./settings.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>FAQ&お問い合わせ</p>
          </div>
          <div class="box__body">
            <div class="faq__header">
              <div class="faq__header__content">
                <p>お客様よりお問い合わせ頂いた中より、よくあるご質問をおまとめいたしました。こちらで解決されない際は「お問い合わせ」ボタンより、メールにてご連絡ください。</p>
              </div>
              <div class="btn--default">
                <a href="#">お問い合わせ</a>
              </div>
            </div>

            <div class="faq__list box--round">
              <div class="faq__list__header box--round__header text-center">
                <p>FAQ</p>
              </div>
              <div class="faq__list__content box--round__body">
                <div class="faq__list__item">
                  <p class="faq__list__item__title">ご利用料金について</p>
                  <p>テキストテキストテキストテキストテキストテキスト</p>
                </div>
                <div class="faq__list__item">
                  <p class="faq__list__item__title">鑑定時間について</p>
                  <p>テキストテキストテキストテキストテキストテキスト</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
</body>
</html>
