<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8">
        <div class="box noborder--bottom--sp">
          <div class="box__header box__header__icon--left--sp">
            <a href="./settings.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>設定</p>
          </div>
          <div class="box__body settings-stack">
            <div class="settings-stack__item">
              <p>ステータスアイコンの表示</p>
              <div class="onoffswitch-wrapper">
                <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch01" checked>
                    <label class="onoffswitch-label on" for="myonoffswitch01"></label>
                </div>
              </div>
            </div>
            <div class="settings-stack__item">
              <p>サウンド設定</p>
              <div class="onoffswitch-wrapper">
                <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch02">
                    <label class="onoffswitch-label off" for="myonoffswitch02"></label>
                </div>
              </div>
            </div>
            <div class="settings-stack__item">
              <p>バイブレーション設定</p>
              <div class="onoffswitch-wrapper">
                <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch03" checked>
                    <label class="onoffswitch-label on" for="myonoffswitch03"></label>
                </div>
              </div>
            </div>
            <div class="settings-stack__item">
              <p>メッセージ通知</p>
              <div class="onoffswitch-wrapper">
                <div class="onoffswitch">
                    <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch04" checked>
                    <label class="onoffswitch-label on" for="myonoffswitch04"></label>
                </div>
              </div>
            </div>

            <div class="settings-stack__item settings-stack__item--lg">
              <p>ポイント消費確認</p>
              <div class="settings-stack__item__row">
                <p>メール送信</p>
                <div class="onoffswitch-wrapper">
                  <div class="onoffswitch">
                      <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch05">
                      <label class="onoffswitch-label off" for="myonoffswitch05"></label>
                  </div>
                </div>
              </div>
              <div class="settings-stack__item__row">
                <p>メッセージ通知</p>
                <div class="onoffswitch-wrapper">
                  <div class="onoffswitch">
                      <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch06" checked>
                      <label class="onoffswitch-label on" for="myonoffswitch06"></label>
                  </div>
                </div>
              </div>
              <div class="settings-stack__item__row">
                <p>画像閲覧</p>
                <div class="onoffswitch-wrapper">
                  <div class="onoffswitch">
                      <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch07">
                      <label class="onoffswitch-label off" for="myonoffswitch07"></label>
                  </div>
                </div>
              </div>
            </div>

            <div class="settings-stack__item">
              <a href="#">サイトから退会</a>
            </div>
          </div> <!-- box__body -->
        </div> <!-- box -->
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
<script src="js/switch.js"></script>
</body>
</html>
