<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
  <?php include "common_header.php"; ?>
  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <div class="col-md-4 hidden-xs">
        <div class="nav--signin">
          <div class="nav--signin__main">
            <input type="text" placeholder="ユーザーID" class="nav--signin__input"/>
            <input type="password" placeholder="パスワード" class="nav--signin__input"/>
            <input type="checkbox" name="signin" value="save" checked id="checkbox01" />
            <label for="checkbox01" class="checkbox">ログイン状態を保持する</label>
            <div class="btn--signin">
              <a href="#">ログイン</a>
            </div>
            <div class="nav--signin__others">
              <a href="#" class="">パスワードを忘れた方</a><br>
              <a href="#">ログインできない方</a><br>
              <a href="./help.php">ヘルプ</a>
            </div>
          </div>
          <div class="nav--signin__sns">
            <div class="nav--signin__sns__item">
              <img src="img/icon_google.jpg">
              <a href="#">Google+アカウントでログイン</a>
            </div>
            <div class="nav--signin__sns__item">
              <img src="img/icon_twitter.png">
              <a href="#">Twitterアカウントでログイン</a>
            </div>
            <div class="nav--signin__sns__item">
              <img src="img/icon_facebook.png">
              <a href="#">Facebookアカウントでログイン</a>
            </div>
          </div>
        </div>
      </div>


      <!-- main -->
      <div class="col-md-8">
        <div class="box--signup">
          <h3>無料新規アカウント登録</h3>
          <div class="box--signup__item">
            <label>お名前</label>
            <input type="text" class="pull-right"></input>
          </div>
          <div class="box--signup__item">
            <label>メールアドレス</label>
            <input type="text" class="pull-right"></input>
          </div>
          <div class="box--signup__item">
            <label>メールアドレス再入力</label>
            <input type="text" class="pull-right"></input>
          </div>
          <div class="box--signup__item">
            <label>パスワードを入力</label>
            <input type="text" class="pull-right"></input>
          </div>
          <div class="box--signup__item birthdate">
            <label>生年月日</label>
            <div class="pull-right">
              <div class="select-box01 birthyear">
                <select>
                  <option value=""></option>
                  <option value="">1895</option>
                  <option value="">1896</option>
                  <option value="">1897</option>
                  <option value="">1898</option>
                  <option value="">1899</option>
                </select>
              </div>
              <p class="text--unit">年</p>
              <div class="select-box01 birthmonth">
                <select>
                  <option value=""></option>
                  <option value="">1</option>
                  <option value="">2</option>
                  <option value="">3</option>
                  <option value="">4</option>
                  <option value="">5</option>
                </select>
              </div>
              <p class="text--unit">月</p>
              <div class="select-box01 birthday">
                <select>
                  <option value=""></option>
                  <option value="">1</option>
                  <option value="">2</option>
                  <option value="">3</option>
                  <option value="">4</option>
                  <option value="">5</option>
                </select>
              </div>
              <p class="text--unit">日</p>
            </div>
          </div>
          <div class="box--signup__item gender">
            <label>性別</label>
            <div class="pull-right">
              <div class="gender-radio">
                <input type="radio" name="gender" value="男" checked id="radio01" />
                <label for="radio01" class="radio">男</label>
              </div>
              <div class="gender-radio gender-radio--woman">
                <input type="radio" name="gender" value="女" id="radio02" />
                <label for="radio02" class="radio">女</label>
              </div>
            </div>
          </div>
          <div class="box--signup__btn">
            <a href="#">新規登録</a>
          </div>
          <div class="box--signup__btn white visible-xs">
            <a href="#">ログイン</a>
          </div>
        </div> <!-- box--signup -->
      </div> <!-- main -->

    </div>
  </div>

  <?php include "common_footer.php"; ?>
<script src="js/switch.js"></script>
</body>
</html>
