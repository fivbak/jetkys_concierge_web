<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8">
        <div class="box">
          <div class="box__header box__header__icon--left--sp">
            <a href="./settings.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>ヘルプ</p>
          </div>
          <div class="box__body">

            <div class="box--round help__list">
              <div class="help__list__header box--round__header text-center">
                <p>ログインボーナスを貰おう！</p>
                <img src="img/arrow.png" class="box--round__header__icon--right">
              </div>
              <div class="help__list__content box--round__body">
                <p>テキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
            <div class="box--round help__list">
              <div class="help__list__header box--round__header text-center">
                <p>アプリ内基本操作</p>
                <img src="img/arrow.png" class="box--round__header__icon--right">
              </div>
              <div class="help__list__content box--round__body">
                <p>テキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
            <div class="box--round help__list">
              <div class="help__list__header box--round__header text-center">
                <p>プロフィール編集</p>
                <img src="img/arrow.png" class="box--round__header__icon--right">
              </div>
              <div class="help__list__content box--round__body">
                <p>テキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
            <div class="box--round help__list">
              <div class="help__list__header box--round__header text-center">
                <p>トーク・チャットをする</p>
                <img src="img/arrow.png" class="box--round__header__icon--right">
              </div>
              <div class="help__list__content box--round__body">
                <p>テキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
            <div class="box--round help__list">
              <div class="help__list__header box--round__header text-center">
                <p>友だち追加</p>
                <img src="img/arrow.png" class="box--round__header__icon--right">
              </div>
              <div class="help__list__content box--round__body">
                <p>テキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
            <div class="box--round help__list">
              <div class="help__list__header box--round__header text-center">
                <p>ブロック</p>
                <img src="img/arrow.png" class="box--round__header__icon--right">
              </div>
              <div class="help__list__content box--round__body">
                <p>テキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
            <div class="box--round help__list">
              <div class="help__list__header box--round__header text-center">
                <p>通報</p>
                <img src="img/arrow.png" class="box--round__header__icon--right">
              </div>
              <div class="help__list__content box--round__body">
                <p>テキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
            <div class="box--round help__list">
              <div class="help__list__header box--round__header text-center">
                <p>連絡先やID交換について</p>
                <img src="img/arrow.png" class="box--round__header__icon--right">
              </div>
              <div class="help__list__content box--round__body">
                <p>テキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
            <div class="box--round help__list">
              <div class="help__list__header box--round__header text-center">
                <p>テキストテキスト</p>
                <img src="img/arrow.png" class="box--round__header__icon--right">
              </div>
              <div class="help__list__content box--round__body">
                <p>テキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
            
          </div>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
<script src="js/jquery-rotate.js"></script>
<script src="js/accordion.js"></script>
</body>
</html>
