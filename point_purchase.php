<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8">
        <div class="box">
          <div class="box__header box__header__icon--left--sp">
            <a href="./settings.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>ポイント購入</p>
          </div>
          <div class="box__body point-purchase">
            <div class="profile__box">
              <div class="profile__box__header">
                <p>所持ポイント</p>
              </div>
              <div class="profile__box__body">
                <p class="profile__name__main">12,345pt</p>
              </div>
            </div>
            <div class="point-purchase__content">
              <h3>購入金額を選択してください。</h3>
              <div class="point-purchase__content__item">
                <input type="radio" name="point" value="500" checked id="radio500" />
                <label for="radio500" class="radio">500円(500pt)</label>
              </div>
              <div class="point-purchase__content__item">
                <input type="radio" name="point" value="1000" id="radio1000" />
                <label for="radio1000" class="radio">1000円(1000pt)</label>
              </div>
              <div class="point-purchase__content__item">
                <input type="radio" name="point" value="2000" id="radio2000" />
                <label for="radio2000" class="radio">2000円(2000pt)</label>
              </div>
              <div class="point-purchase__content__item">
                <input type="radio" name="point" value="5000" id="radio5000" />
                <label for="radio5000" class="radio">5000円(5000pt)</label>
              </div>
              <div class="point-purchase__content__item">
                <input type="radio" name="10000" value="10000" id="radio10000" />
                <label for="radio10000" class="radio">10000円(10000pt)</label>
              </div>
            </div>
          </div> <!-- box__body -->
          <div class="btn--default mg-top-10">
            <a href="#">購入</a>
          </div>
        </div> <!-- box -->
        <div class="btn-back hidden-xs">
          <a href="#">戻る</a>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
</body>
</html>
