<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8 inbox">
        <div class="box noborder--bottom--sp">
          <div class="box__header hidden-xs">
            <p>▶里中 今日子</p>
          </div>
          <div class="box__header box__header__icon--left--sp visible-xs">
            <a href="./mails.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>里中 今日子</p>
            <a href="#">
              <div class="box__header__icon--right visible-xs box__header__icon--comment auto-width">
                <img src="img/comment.png">
              </div>
            </a>
          </div>
          <div class="box__body compose-mail">
            <div class="box__body compose-mail__container">
              <input class="compose-mail__title" placeholder="タイトル">
              <textarea class="compose-mail__content" placeholder="内容を入力してください。"/></textarea>
            </div>
            <div class="compose-mail__attachments hidden-xs">
              <a href="#">
                <img src="img/icon_camera.png">
              </a>
              <a href="#">
                <img src="img/icon_video.png">
              </a>
            </div>
          </div>
          <div class="inbox__mail__reply hidden-xs">
            <a href="./mail_confirm.php">確定</p></a>
          </div>
        </div>
      </div> <!-- main -->
      <div class="visible-xs compose-mail__footer">
        <a href="#" class="compose-mail__footer__camera">
          <img src="img/icon_camera.png">
        </a>
        <a href="./mail_confirm.php" class="compose-mail__footer__action pull-right">確定</p></a>
      </div>

    </div>
  </div>

<?php include "common_footer.php"; ?>
<script src="js/tab.js"></script>
</body>
</html>
