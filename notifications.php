<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8">
        <div class="box noborder--bottom--sp">
          <div class="box__header box__header__icon--left--sp">
            <a href="./settings.php" class="visible-xs-inline-block"><img src="img/arrow_left.png" class="box__header--arrow--left visible-xs-inline-block"></a>
            <p>お知らせ</p>
          </div>
          <div class="box__body list">
            <div class="list__notification">
              <div class="list__notification__icon">
                <img src="img/icon_notice.png">
              </div>
              <div class="list__notification__content">
                <h3>2016/1/1  ◯◯についてのお知らせ</h3>
                <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
            <div class="list__notification">
              <div class="list__notification__icon">
              </div>
              <div class="list__notification__content">
                <h3>2016/1/1  ◯◯についてのお知らせ</h3>
                <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
            <div class="list__notification">
              <div class="list__notification__icon">
              </div>
              <div class="list__notification__content">
                <h3>2016/1/1  ◯◯についてのお知らせ</h3>
                <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
              </div>
            </div>
          </div> <!-- box__body -->
        </div> <!-- box -->
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
</body>
</html>
