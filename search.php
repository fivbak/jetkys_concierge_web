<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8">
        <div class="box search noborder--bottom--sp">
          <div class="box__header">
            <p>検索</p>
          </div>
          <div class="box__body">
            <div class="box__body__label">
              <label>ジャンル</label>
            </div>
            <div class="search__condition">
              <div class="row">
                <div class="col-sm-6 col-xs-5">
                  <div class="search__condition__item search__condition__item--left">
                    <input type="checkbox" name="love" value="恋愛" checked id="checkbox01" />
                    <label for="checkbox01" class="checkbox">恋愛</label>
                  </div>
                </div>
                <div class="col-sm-6 col-xs-7">
                  <div class="search__condition__item">
                    <input type="checkbox" name="family" value="家族、子育て" id="checkbox02" />
                    <label for="checkbox02" class="checkbox">家族、子育て</label>
                  </div>
                </div>
                <div class="col-sm-6 col-xs-5">
                  <div class="search__condition__item search__condition__item--left">
                    <input type="checkbox" name="work" value="仕事" id="checkbox03" />
                    <label for="checkbox03" class="checkbox">仕事</label>
                  </div>
                </div>
                <div class="col-sm-6 col-xs-7">
                  <div class="search__condition__item">
                    <input type="checkbox" name="money" value="お金" id="checkbox04" />
                    <label for="checkbox04" class="checkbox">お金</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="box__body__label box__body__label--second">
              <label>性別</label>
            </div>
            <div class="search__condition search__condition--second">
              <div class="row">
                <div class="col-sm-6 col-xs-5">
                  <div class="search__condition__item search__condition__item--left">
                    <input type="radio" name="gender" value="男" checked id="radio01" />
                    <label for="radio01" class="radio">男</label>
                  </div>
                </div>
                <div class="col-sm-6 col-xs-7">
                  <div class="search__condition__item">
                    <input type="radio" name="gender" value="女" id="radio02" />
                    <label for="radio02" class="radio">女</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="btn--default">
              <a href="./search_result.php">検索</a>
            </div>
          </div>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
</body>
</html>
