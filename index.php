<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_login_check.php"; ?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8 top-main">
        <div class="top-main__news">
          <div class="top-main__news__item">
            <p class="date">2月26日</p>
            <a href="#">テキストテキストテキストテキスト</a>
          </div>
          <div class="top-main__news__item">
            <p class="date">2月26日</p>
            <a href="#">テキストテキストテキストテキスト</a>
          </div>
          <div class="top-main__news__item">
            <p class="date">2月26日</p>
            <a href="#">テキストテキストテキストテキスト</a>
          </div>
          <div class="top-main__news__item">
            <p class="date">2月26日</p>
            <a href="#">テキストテキストテキストテキスト</a>
          </div>
        </div>

        <div class="top-main__genre">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="top-main__genre__btn">
                <a href="#">
                  <img src="img/btn_feeling.png">
                </a>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="top-main__genre__btn">
                <a href="#">
                  <img src="img/btn_uwaki.png">
                </a>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="top-main__genre__btn">
                <a href="#">
                  <img src="img/btn_tech.png">
                </a>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="top-main__genre__btn">
                <a href="#">
                  <img src="img/btn_grow.png">
                </a>
              </div>
            </div>
          </div>
        </div>

        <div class="top-main__users">
          <div class="row">
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-4 col-xs-6">
              <a href="#">
                <div class="top-main__users__item">
                  <div class="top-main__users__item__picture">
                    <img src="img/icon_woman_60.png">
                  </div>
                  <div class="top-main__users__item__names">
                    <p>名前</p>
                    <p>テキストテキストテキスト</p>
                  </div>
                  <div class="top-main__users__item__text">
                    <p>テキストテキストテキストテキスト</p>
                  </div>
                </div>
              </a>
            </div>
            <div class="col-md-12">
              <a href="#" class="top-main__users__more pull-right">もっとみる</a>
            </div>
          </div>
        </div>
        <div class="top-main__comic">
          <p>マンガ①</p>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
</body>
</html>
