<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8">
        <div class="box noborder--bottom--sp">
          <div class="box__header">
            <p>プロフィール確認</p>
          </div>
          <div class="box__body profile">
            <div class="row">
              <div class="col-md-4 col-xs-5">
                <div class="profile__picture">
                  <img src="img/profile_default.png">
                </div>
              </div>

              <div class="col-md-8 col-xs-7">
                <div class="profile__name profile__box">
                  <div class="profile__box__header">
                    <p>サリエンシーネーム</p>
                  </div>
                  <div class="profile__box__body">
                    <p class="profile__name__main">名前名前名前</p>
                    <p class="profile__name__sub">27歳／女</p>
                  </div>
                </div>
                <div class="profile__license profile__box">
                  <div class="profile__box__header">
                    <p>資格／検定</p>
                  </div>
                  <div class="profile__box__body">
                    <p class="profile__license__text">
                      △△免許　　◯◯検定１級<br/>
                      △△免許　　◯◯検定１級
                    </p>
                  </div>
                </div>
              </div> <!-- col-md-8 -->

              <div class="col-md-12 col-xs-12">
                <div class="profile__description profile__box">
                  <div class="profile__box__header">
                    <p>ジャンル</p>
                  </div>
                  <div class="profile__box__body profile__box__body--genre">
                    <p>・恋愛<br>・家族、子育て</p>
                  </div>
                </div>

                <div class="profile__description profile__box">
                  <div class="profile__box__header">
                    <p>プロフィール</p>
                  </div>
                  <div class="profile__box__body">
                    <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                  </div>
                </div>
                <a href="#">
                  <div class="btn--divination">
                    <p class="btn--divination__title">◯◯◯鑑定</p>
                    <div class="btn--divination__round"></div>
                    <div class="btn--divination__price">
                      <img src="img/price.png">
                    </div>
                  </div>
                </a>
              </div> <!-- col-md-12 -->
            </div> <!-- row -->

          </div> <!-- box__body -->
          <div class="box__footer share-on-sns">
            <p>SNSで共有</p>
            <a href="#">
              <img src="img/icon_twitter.png">
            </a>
            <a href="#">
              <img src="img/icon_facebook.png">
            </a>
          </div>
        </div> <!-- box -->
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
</body>
</html>
