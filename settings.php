<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8 my-saliency">
        <div class="box noborder--bottom--sp">
          <div class="box__header">
            <p>設定</p>
          </div>
          <div class="box__tab">
            <div class="box__tab__item box__tab__item--active box__tab--first">
              <p class="font--10--sp">ユーザーホーム</p>
            </div>
            <div class="box__tab__item box__tab--second">
              <p class="font--10--sp">サリエンシーホーム</p>
            </div>
          </div>

          <div class="box__body profile settings box__body--first">
            <div class="row">
              <div class="col-md-4 col-xs-5">
                <div class="profile__picture">
                  <img src="img/profile_default.png">
                  <a href="#">
                    <img class="profile__picture__btn" src="img/btn_plus.png">
                  </a>
                </div>
              </div>

              <div class="col-md-8 col-xs-7">
                <div class="profile__name profile__box">
                  <div class="profile__box__header">
                    <p>ユーザーネーム</p>
                  </div>
                  <div class="profile__box__body">
                    <p class="profile__name__main">名前名前名前</p>
                    <p class="profile__name__id">27歳／女</p>
                  </div>
                </div>
                <div class="profile__point profile__box">
                  <div class="profile__box__header">
                    <p>所持ポイント</p>
                  </div>
                  <div class="profile__box__body">
                    <p class="profile__point__text">12,345pt</p>
                  </div>
                </div>
                <a href="#" class="anchor--edit-profile pull-right">プロフィール編集</a>
              </div> <!-- col-md-8 -->

              <div class="col-md-12 col-xs-12">
                <div class="btn--default btn--round">
                  <a href="">ポイント購入</a>
                </div>

                <div class="menu--stack">
                  <div class="menu--stack__item">
                    <img src="img/icon_notice.png">
                    <a href="#">お知らせ</a>
                  </div>
                  <div class="menu--stack__item">
                    <a href="./settings.php">設定</a>
                  </div>
                  <div class="menu--stack__item">
                    <a href="./faq.php">FAQ&お問い合わせ</a>
                  </div>
                  <div class="menu--stack__item">
                    <a href="./help.php">ヘルプ</a>
                  </div>
                  <div class="menu--stack__item">
                    <a href="#">利用規約</a>
                  </div>
                  <div class="menu--stack__item">
                    <a href="#">プライバシーポリシー</a>
                  </div>
                </div>
              </div> <!-- col-md-12 -->
            </div> <!-- row -->
          </div>

          <div class="box__body profile settings box__body--second">
            <div class="row">
              <div class="col-md-4 col-xs-5">
                <div class="profile__picture">
                  <img src="img/profile_default.png">
                  <a href="#">
                    <img class="profile__picture__btn" src="img/btn_plus.png">
                  </a>
                </div>
              </div>

              <div class="col-md-8 col-xs-7">
                <div class="profile__name profile__box">
                  <div class="profile__box__header">
                    <p>ユーザーネーム</p>
                  </div>
                  <div class="profile__box__body">
                    <p class="profile__name__main">名前名前名前</p>
                    <p class="profile__name__id">27歳／女</p>
                  </div>
                </div>
                <div class="profile__point profile__box">
                  <div class="profile__box__header">
                    <p>所持ポイント</p>
                  </div>
                  <div class="profile__box__body">
                    <p class="profile__point__text">12,345pt</p>
                  </div>
                </div>
                <a href="./profile.php" class="anchor--edit-profile anchor--confirm-profile pull-right">プロフィール確認</a>
                <span class="anchor--edit-profile--separator pull-right">／</span>
                <a href="#" class="anchor--edit-profile pull-right">プロフィール編集</a>
              </div> <!-- col-md-8 -->

              <div class="col-md-12 col-xs-12">
                <div class="btn--default btn--round">
                  <a href="">ポイント購入</a>
                </div>

                <div class="menu--stack">
                  <div class="menu--stack__item">
                    <img src="img/icon_notice.png">
                    <a href="#">お知らせ</a>
                  </div>
                  <div class="menu--stack__item">
                    <a href="./settings.php">設定</a>
                  </div>
                  <div class="menu--stack__item">
                    <a href="./faq.php">FAQ&お問い合わせ</a>
                  </div>
                  <div class="menu--stack__item">
                    <a href="./help.php">ヘルプ</a>
                  </div>
                  <div class="menu--stack__item">
                    <a href="#">利用規約</a>
                  </div>
                  <div class="menu--stack__item">
                    <a href="#">プライバシーポリシー</a>
                  </div>
                </div>
              </div> <!-- col-md-12 -->
            </div> <!-- row -->
          </div>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
<script src="js/tab.js"></script>
</body>
</html>
