<?php
require 'common_include.php';
$title="";

$url = API_PATH;
?>
<?php include "common_head.php"; ?>
</head>
<body>
<?php include "common_header.php"; ?>

  <div class="container">
    <?php include "common_tab_menu.php"; ?>

    <div class="row">
      <?php include "common_sidenav.php"; ?>


      <!-- main -->
      <div class="col-md-8 my-saliency">
        <div class="box">
          <div class="box__header">
            <p>マイサリエンシー</p>
            <img src="img/arrow.png" class="box__header__icon--right">
          </div>
          <div class="box__body list">
            <a href="#">
              <div class="list__users">
                <div class="list__users__picture">
                  <img src="img/icon_woman_60.png">
                </div>
                <div class="list__users__names">
                  <p class="name">里中 今日子</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__users__comment">
                  <img src="img/icon_comment_lg.png">
                  <div class="notice">
                    <p>5</p>
                  </div>
                </div>
                <div class="list__users__mail">
                  <img src="img/icon_mail_lg.png">
                  <div class="notice">
                    <p>5</p>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
        <div class="box">
          <div class="box__header">
            <p>マイユーザー</p>
            <img src="img/arrow.png" class="box__header__icon--right">
          </div>
          <div class="box__body list">
            <a href="#">
              <div class="list__users">
                <div class="list__users__picture">
                  <img src="img/icon_woman_60.png">
                </div>
                <div class="list__users__names">
                  <p class="name">里中 今日子</p>
                  <p>テキストテキストテキストテキスト</p>
                </div>
                <div class="list__users__comment">
                  <img src="img/icon_comment_lg.png">
                  <div class="notice">
                    <p>5</p>
                  </div>
                </div>
                <div class="list__users__mail">
                  <img src="img/icon_mail_lg.png">
                  <div class="notice">
                    <p>5</p>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </div>
      </div> <!-- main -->

    </div>
  </div>

<?php include "common_footer.php"; ?>
</body>
</html>
