<div class="row tab-menu">
      <div class="col-sm-2 col-xs-2">
        <a href="./index.php">
          <div class="tab-menu-item tab-menu-active">
            <p>TOP</p>
          </div>
        </a>
      </div>
      <div class="col-sm-2 col-xs-2">
        <a href="./my_saliency.php">
          <div class="tab-menu-item">
            <p>マイサリエンシ―</p>
          </div>
        </a>
      </div>
      <div class="col-sm-2 col-xs-2">
        <a href="./messages.php">
          <div class="tab-menu-item">
            <p>メッセージ</p>
          </div>
        </a>
      </div>
      <div class="col-sm-2 col-xs-2">
        <a href="./mails.php">
          <div class="tab-menu-item">
            <p>メール</p>
          </div>
        </a>
      </div>
      <div class="col-sm-2 col-xs-2">
        <a href="./search.php">
          <div class="tab-menu-item">
            <p>検索</p>
          </div>
        </a>
      </div>
      <div class="col-sm-2 col-xs-2">
        <a href="./settings.php">
          <div class="tab-menu-item">
            <p>設定</p>
          </div>
        </a>
      </div>
    </div><!-- tab-menu -->